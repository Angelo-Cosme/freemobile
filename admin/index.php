<?php

include 'functions/dbconnect.php';

$pages = scandir('pages/');
if(isset($_GET['page']) && !empty($_GET['page'])){
    if(in_array($_GET['page'].'.php',$pages)){
        $page = $_GET['page'];
    }else{
        $page = "error";
    }
}else{
    $page = "home";
}


$pages_functions = scandir('functions/');
if(in_array($page.'.func.php',$pages_functions)){
    include 'functions/'.$page.'.func.php';
}

?>

<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf8" />
        
        <link type="text/css" rel="stylesheet" href="css/materialize.css"  media="screen,projection"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style type="text/css">
            .icon_style{
            position: absolute;
            right: 10px;
            top: 10px;
            font-size: 20px;
            color: white;
            cursor:pointer; 
            }
        </style>
    </head>
    <body>
        <?php
            if($page != 'login' && !isset($_SESSION['admin'])){
                header("Location:index.php?page=login");
            }
        ?>
        
        <?php include 'body/topbar.php'; ?>
        
        <div class="container">
            <?php
                include'pages/'.$page.'.php';
            ?>
        </div>
        
        <!--Import jQuery before materialize.js-->
     
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/materialize.js"></script>
        <script type="text/javascript" src="js/script.js"></script>
        
        
        
        <!--Import functions.js-->
        <?php
            $pages_js = scandir('js/');
            if(in_array($page.'.func.js',$pages_js)){
                ?>
                    <script type="text/javascript" src="js/<?= $page ?>.func.js"></script>
                <?php
            }

        ?>
    </body>
</html>