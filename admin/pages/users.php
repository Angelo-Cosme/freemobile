<?php
    if(admin()!='1'){
        header("Location:index.php?php=home");
    }
?>

<h4 class="text center">Paramètre / Ajout utilisateurs</h4>

<div class="row">
    <div class="col m6 s12">
        <h5>Utilisateurs</h5>
        <table>
            <thead>
                <tr>
                    <th>Nom d'utilisateur</th>
                    <th>Rôle</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $users = get_user();
                    
                    foreach ($users as $user) {
                        ?>
                        <tr id="user_<?= $user->id ?> ?>">
                            <td><?= $user->username ?></td>
                            <td><?= $user->role ?></td>
                            <td>
                                <a href="#" id="<?= $user->id ?>" class="btn-floating btn-small waves-affect waves-light red delete_user">S</a>
                            </td>
                        </tr>
                        <?php
                    }
                
                
                ?>
            </tbody>
        </table>
        <p><strong>Veuillez actualiser la page après avoir cliquer sur le bouton SUPPRIMER</strong></p>
    </div>
    
    <div class="col m6 s12">
        
            <?php
                if(isset($_POST['user'])){
                    $username = htmlspecialchars($_POST['username']);
                    $password = htmlspecialchars($_POST['password']);
                    $role = htmlspecialchars($_POST['role']);
                    
                    $errors = [];
                    
                    if(empty($username) || empty($password) || empty($role)){
                        $errors[] = "Veuillez remplir tous les champs";
                    }
                    
                    if(username_exist($username) == 1){
                        $errors[] = "Cet utilisateur existe déjà";
                    }
                    
                    if(strlen($password) < 7){
                        $errors[] = "Mot de passe trop court. 7 caractères minimum";
                    }
                    
                    else if(!empty($username) || !empty($password) ||
                            !empty($role)){
                        add_user($username,$password,$role);
                        $errors[] = "Cet utilisateur a été bien ajouté";
                        
                        header("Location: /FreeMobile/admin/index.php?page=users");

                    }
                }
                
                if(!empty($errors)){
                        ?>
                        <div class="card green">
                            <div class="card-content white-text">
                                <?php
                                    foreach ($errors as $error) {
                                         echo $error."<br/>";                   
                                    }
                                
                                ?>
                                <i class="material-icons icon_style" id="alert_close" aria-hidden="true">clear</i>
                            </div>
                        </div>
                        <?php
                    }
            ?>
        
        <h5>Ajouter un utilisateur</h5>
        <form method="POST" autocomplete="off">
            <div class="row">
                <input class="input-field col s12" type="text" name="username" placeholder="Nom d'utilisateur">
            </div>
            
            <div class="row">
                <input class="input-field col s12" type="password" name="password" placeholder="Mot de passe">
            </div>
            
            <div class="row">
                <select name="role" type="text" id="role">
                    <option value="" disabled selected>sélectionnez le type d'utilisateur</option>
                    <option value="admin">admin</option>
                    <option value="editeur">editeur</option>
                </select>
            </div>
            
            <center><div><button type="submit" name="user" class="waves-affect waves-light btn light-blue">Insérer</button></div></center>
        </form>
    </div>
   
</div>
 <pre>
    <?php
        if(!isset($_POST['user'])){
          $_SESSION["mgs"] = null;
     }
            
    ?>
</pre>