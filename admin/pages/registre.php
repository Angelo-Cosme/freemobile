
<?php 
include 'pagination.php'; 

?>

<div>

<h4 class="text center">Liste des opérations</h4>

<div class="row">
    <form action="" method="GET">
        <div class="input-field col s4">
            <select name="sp">
                <option value="" disabled selected>Le nombre de liste à afficher</option>
                <option value="3">3</option>
                <option value="10">10</option>
                <option value="15">15</option>
                <option value="20">20</option>
            </select>
        </div>
        <input type="hidden" name="p" value="<?php echo $current; ?>">
        <div class="input-field col s2">
        <button type="submit" class="waves-affect waves-light btn light-blue">Appliqué</button>
        </div>
    </form>

    <div class="input-field col s6">
        <input type="text" id="search" name="search" placeholder="Entrez le numéro que vous recherchez ici...">
    </div>
</div>

 <!--- REGISTRE  -->
 <table class="responsive-table">
    <thead>
        <tr>
            <th>Date Oprt</th>
            <th>Numéro</th>
            <th>Type_Oprt</th>
            <th>Montant</th>
            <th>Identité Client</th>
            <th>Réseau</th>
            <th>N°Pièce</th>
            <th>Type de Pièce</th>
            <th>Expire Piece</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php
            
               while ($regs = $reqReg->fetch()) {
                ?>
               <tr id="register_<?php echo $regs['id']; ?>">
                    <td><?php echo $regs['date_operation']; ?></td>
                    <td><?php echo $regs['phone']; ?></td>
                    <td><?php echo $regs['types_operations']; ?></td>
                    <td><?php echo $regs['montant']; ?></td>
                    <td><?php echo $regs['nom_prenom']; ?></td>
                    <td><?php echo $regs['network']; ?></td>
                    <td><?php echo $regs['ref_piece_id']; ?></td>
                    <td><?php echo $regs['type_piece']; ?></td>
                    <td><?php echo $regs['expire_piece']; ?></td>
                    <td>
                        <a class="btn-floating btn-small waves-affect waves-light blue" href="index.php?page=update&id=<?php echo $regs['id']; ?>">M</a>
                        <a href="#" id="<?php echo $regs['id']; ?>" class="btn-floating btn-small waves-affect waves-light red delete_register <?php if(admin()!= '1'){echo "disabled";} ?>">S</a>
                    </td>
                   
                </tr>
                        
            <?php
            }
        
        ?>
    </tbody>
</table>
</div>
 <!---/ REGISTRE  -->
 
<!--- PAGINATION  -->
   <ul class="pagination text center">
       <li class="<?php if($current == '1'){echo "disabled";} ?>"><a href="index.php?page=registre&p=<?php if($current != '1'){echo $current-1;}else{echo $current;} ?>">&laquo;</a></li>
       
    
    <?php 
    for($i=1; $i<=$nbPage; $i++){
        
        if($i == $current){
            ?>
            <li class="active"><a href="index.php?page=registre&p=<?php echo $i ?>"><?php echo $i ?></a></li>
            <?php
        }else{ 
            ?>
            <li><a href="index.php?page=registre&p=<?php echo $i ?>"><?php echo $i ?></a></li>
            <?php
        }
        
        }
        ?>
    
        <li class="<?php if($current == $nbPage){echo "disabled";} ?>"><a href="index.php?page=registre&p=<?php if($current != $nbPage){echo $current+1;}else{echo $current;} ?>">&raquo;</a></li>
        <?php
    
    ?>
    
    <li></li>
  </ul> 
<!---/ PAGINATION  -->
