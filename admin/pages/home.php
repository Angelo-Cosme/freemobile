

<h2 class="text center"> Tableau de bord</h2>


<div class="row">
    <?php
    
        $tables = [
            "Nbre d'enregistrements" => "register",
            "Nbre d'Utilisateurs" => "users"
        ];
        
        $colors = [
            "register" => "blue",
            "users" => "red"
        ];
    
    ?>
    
    
    <?php
    foreach ($tables as $table_name => $table) {
        ?>
        <div class="col l6 m6 s12">
            <div class="card">
                <div class="card-content <?= getColor($table, $colors) ?>">
                    <span class="card-title"><?= $table_name ?></span>
                    <?php $nbrInTable = inTable($table); ?>
                    <h4><?= $nbrInTable[0] ?></h4>
                </div>
            </div>
        </div>
    
        <?php
    }
    ?>

</div>

<div class="row">
    <div class="col s12">
        <div class="card-panel">
            <h4 class="text center"> Enregistrez vos opérations</h4>
            
            <?php
                if(isset($_POST['register'])){
                    $phone = htmlspecialchars($_POST['phone']);
                    $types_operations = htmlspecialchars($_POST['types_operations']);
                    $montant = htmlspecialchars($_POST['montant']);
                    $nom_prenom = htmlspecialchars($_POST['nom_prenom']);
                    $network = htmlspecialchars($_POST['network']);
                    $ref_piece_id = htmlspecialchars($_POST['ref_piece_id']);
                    $type_piece = htmlspecialchars($_POST['type_piece']);
                    $expire_piece = htmlspecialchars($_POST['expire_piece']);
                    $date_operation = htmlspecialchars($_POST['date_operation']);
                    
                    $errors = [];
                    
                    if(empty($phone) || empty($types_operations) || empty($montant) 
                            || empty($nom_prenom) || empty($network) || empty($ref_piece_id) 
                            || empty($type_piece) || empty($expire_piece) || empty($date_operation)){
                        
                            $errors['empty'] = "Veuillez remplier tous les champs";
                    }
                    
                    if(strlen($phone) > 12){
                        $errors[] = "Numéro saisi incorrect. 12 caratères maximum";
                    }
                    
                    elseif(!empty($phone) || !empty($types_operations) || !empty($montant) 
                            || !empty($nom_prenom) || !empty($network) || !empty($ref_piece_id) 
                            || !empty($type_piece) || !empty($expire_piece) || !empty($date_operation)){
                        
                        add_reg($phone,$types_operations,$montant,$nom_prenom,$network,$ref_piece_id,$type_piece,$expire_piece,$date_operation);
                        
                        $errors[] = "Enregistrement  de l'opération sur le $phone bien effectué";
                        
                        $_SESSION["msg"] = $errors;
                        
                        header("Location: /FreeMobile/admin/");
 
                    }
                }
                    if(!empty($_SESSION["msg"])){
                    ?>
                        <div class="card-alert card red">
                            <div class="card-content white-text">
                                <?php
                                foreach($_SESSION["msg"] as $error){
                                    echo $error."<br/>";
                                }
                                ?>
                                <i class="material-icons icon_style" id="alert_close" aria-hidden="true">clear</i>
                            </div>
                        </div>
                    <?php
                }
  
            ?>
            
          <form method="POST">
            <div class="row">
                <div class="input-field col s6">
                    <input type="text" id="phone" name="phone" placeholder="Entrez le numéro...">
                </div>

                <div class="input-field col s6">
                    <select name="types_operations" type="text" id="types_operations">
                        <option value="" disabled selected>Choisissez le type d'opération</option>
                        <option value="retrait">Retrait</option>
                        <option value="Depot">Dépôt</option>
                    </select>
                </div>
                
                <div class="input-field col s6">
                    <input type="text" id="montant" name="montant" placeholder="Montant de l'opération...">
                </div>
                
                <div class="input-field col s6">
                    <input type="text" id="nom_prenom" name="nom_prenom" placeholder="Nom et Prénom du client...">
                </div>
                
                <div class="input-field col s6">
                    <select name="network" type="text" id="network">
                        <option value="" disabled selected>Choisissez le type de réseaux</option>
                        <option value="Mobile Money MTN">Mobile Money/MTN</option>
                        <option value="Flooz MOOV">Flooz/MOOV</option>
                    </select>
                </div>
                
                <div class="input-field col s6">
                    <input type="text" id="ref_piece_id" name="ref_piece_id" placeholder="Numéro de référence piece...">
                </div>
                
                <div class="input-field col s6">
                    <select name="type_piece" type="text" id="type_piece">
                        <option value="" disabled selected>Le type de pièce</option>
                        <option value="Carte d'identite Nationale">Carte d'identité Nationale</option>
                        <option value="Passport">Passport</option>
                        <option value="RAVIP">RAVIP</option>
                        <option value="Permis de conduit">Permis de conduit</option>
                        <option value="LEPI">LEPI</option>
                        <option value="Carte Professionnelle">Carte Professionnelle</option>
                        <option value="Autre">Autre</option>
                    </select>
                </div> 
                
                <div class="input-field col s6">
                    <input type="date" id="expire_piece" name="expire_piece" placeholder="Date d'expiration de la pièce...">
                    <label>Date d'expiration de la pièce</label>
                </div>
                    
                <div class="input-field col s6">
                    <input type="date" id="date_operation" name="date_operation" placeholder="Date d'opération / Enregistrement...">
                    <label>Date d'opération / Enregistrement</label>
                </div>
                
                <center>
                 <div class="input-field col s6">
                    <button type="submit"  name="register" class="waves-affect waves-light btn light-blue">
                        Enregistré
                    </button>
                </center>
                </div>
            </form>    
            </div>

            </div>
        
        </div>
    </div>
</div>