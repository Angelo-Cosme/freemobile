<?php
    if(isset($_SESSION['admin'])){
        header("Location:index.php?page=login");
    }
?>



<div class="row">
    <div class="col l6 m6 s12 offset-l3 offset-m3">
        <div class="card-panel">
            <div class="row">
                <div class="col s6 offset-s3">
                    <img src="../img/admin.png" alt="admin" width="100%">
                </div>
            </div>
            <h4 class="center-align">Se connecter</h4>
            
            <?php
                if(isset($_POST['cnx'])){
                    $username = htmlspecialchars(trim($_POST['username']));
                    $password = htmlspecialchars(trim($_POST['password']));
                    
                    $errors = [];
                    
                    if(empty($username) || empty($password)){
                        $errors['empty'] = "Tous les champs doivent être remplis";
                    }else if(is_admin($username,$password) == 0){
                        $errors['exist'] = "Cet utilisateur n'existe pas dans le système";
                    }
                    
                    if(!empty($errors)){
                        ?>
                        <div class="card red">
                            <div class="card-content white-text">
                                <?php
                                    foreach ($errors as $error) {
                                         echo $error."<br/>";                   
                                    }
                                
                                ?>
                                <i class="material-icons icon_style" id="alert_close" aria-hidden="true">clear</i>
                            </div>
                        </div>
                        <?php
                    }else{
                        $_SESSION['admin'] = $username;
                        header("Location:index.php?page=home");
                    }
                }
            
            
            
            ?>
            
            
            
            
            <form method="POST">
                <div class="row">
                    <div class="input-field col s12">
                        <input type="text" id="email" name="username" placeholder="Nom d'utilisateur"> 
                    </div>
                    
                    <div class="input-field col s12">
                        <input type="password" id="password" name="password" placeholder="Mot de passe">
                    </div>    
                </div>
                
                <center>
                    <button type="submit"  name="cnx" class="waves-affect waves-light btn light-blue">
                    Connexion
                    </button>
                </center>    
                <br/>
            </form>
        </div>
    </div>
</div>