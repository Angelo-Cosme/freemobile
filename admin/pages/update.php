
<?php
    $regt = get_reg();
    if($regt == false){
        header("Location:index.php?page=error");
    }


?>
<br><br>
<div class="row">
    <div class="col s12">
        <div class="card-panel">
            
            <h5 class="text center">Modification de l'enregistrement sur le <strong><?= $regt->phone ?></strong></h5>
            
            <?php
                if(isset($_POST['update'])){
                    $phone = htmlspecialchars($_POST['phone']);
                    $types_operations = htmlspecialchars($_POST['types_operations']);
                    $montant = htmlspecialchars($_POST['montant']);
                    $nom_prenom = htmlspecialchars($_POST['nom_prenom']);
                    $network = htmlspecialchars($_POST['network']);
                    $ref_piece_id = htmlspecialchars($_POST['ref_piece_id']);
                    $type_piece = htmlspecialchars($_POST['type_piece']);
                    $expire_piece = htmlspecialchars($_POST['expire_piece']);
                    $date_operation = htmlspecialchars($_POST['date_operation']);
                    
                    $errors = [];
                    
                    if(empty($phone) || empty($types_operations) || empty($montant) 
                            || empty($nom_prenom) || empty($network) || empty($ref_piece_id) 
                            || empty($type_piece) || empty($expire_piece) || empty($date_operation)){
                        
                            $errors['empty'] = "Veuillez remplir tous les champs";
                    }
                    
                    elseif(!empty($phone) || !empty($types_operations) || !empty($montant) 
                            || !empty($nom_prenom) || !empty($network) || !empty($ref_piece_id) 
                            || !empty($type_piece) || !empty($expire_piece) || !empty($date_operation)){
                        
                        update($phone,$types_operations,$montant,$nom_prenom,$network,$ref_piece_id,$type_piece,$expire_piece,$date_operation,$_GET['id']);
                        
                        $errors[] = "Votre modification a été bien effectué sur le $phone";
                        
                        $_SESSION["mg"] = $errors;
                        
                       ?>
                         <script>
                            window.location.replace("index.php?page=update&id=<?= $_GET['id'] ?>");
                         </script>

                        <?php
 
                    }
                }
                
                if(!empty($_SESSION["mg"])){
                    ?>
                        <div class="card green">
                            <div class="card-content white-text">
                                <?php
                                foreach($_SESSION["mg"] as $error){
                                    echo $error."<br/>";
                                }
                                ?>
                                <i class="material-icons icon_style" id="alert_close" aria-hidden="true">clear</i>
                            </div>
                        </div>
                    <?php
                }else{
                    
                    
                    }
            
            ?>
            
            
            <form method="POST">
                <div class="row">
                    <div class="input-field col s6">
                        <input type="text" id="phone" name="phone" placeholder="Entrez le numéro..." value="<?= $regt->phone ?>">
                    </div>

                    <div class="input-field col s6">
                        <select name="types_operations" type="text" id="types_operations" value="<?= $regt->types_operations ?>">
                            <option value="" disabled selected>Choisissez le type d'opération</option>
                            <option value="retrait">Retrait</option>
                            <option value="Depot">Dépôt</option>
                        </select>
                    </div>

                    <div class="input-field col s6">
                        <input type="text" id="montant" name="montant" placeholder="Montant de l'opération..." value="<?= $regt->montant ?>">
                    </div>

                    <div class="input-field col s6">
                        <input type="text" id="nom_prenom" name="nom_prenom" placeholder="Nom et Prénom du client..." value="<?= $regt->nom_prenom ?>">
                    </div>

                    <div class="input-field col s6">
                        <select name="network" type="text" id="network" value="<?= $regt->network ?>">
                            <option value="" disabled selected>Choisissez le type de réseaux</option>
                            <option value="Mobile Money MTN">Mobile Money/MTN</option>
                            <option value="Flooz MOOV">Flooz/MOOV</option>
                        </select>
                    </div>

                    <div class="input-field col s6">
                        <input type="text" id="ref_piece_id" name="ref_piece_id" placeholder="Numéro de référence piece..." value="<?= $regt->ref_piece_id ?>">
                    </div>

                    <div class="input-field col s6">
                        <select name="type_piece" type="text" id="type_piece" value="<?= $regt->type_piece ?>">
                            <option value="" disabled selected>Le type de pièce</option>
                            <option value="Carte d'identite Nationale">Carte d'identité Nationale</option>
                            <option value="Passport">Passport</option>
                            <option value="RAVIP">RAVIP</option>
                            <option value="Permis de conduit">Permis de conduit</option>
                            <option value="LEPI">LEPI</option>
                            <option value="Carte Professionnelle">Carte Professionnelle</option>
                            <option value="Autre">Autre</option>
                        </select>
                    </div> 

                    <div class="input-field col s6">
                        <input type="date" id="expire_piece" name="expire_piece" placeholder="Date d'expiration de la pièce..." value="<?= $regt->expire_piece ?>">
                        <label>Date d'expiration de la pièce</label>
                    </div>

                    <div class="input-field col s6">
                        <input type="date" id="date_operation" name="date_operation" placeholder="Date d'opération / Enregistrement..." value="<?= $regt->date_operation ?>">
                        <label>Date d'opération / Enregistrement</label>
                    </div>

                    <center>
                     <div class="input-field col s6">
                        <button type="submit"  name="update" class="waves-affect waves-light btn light-blue">
                            Modifier
                        </button>
                    </center>
                </div>
            </form>
        </div>
    </div>
</div>
