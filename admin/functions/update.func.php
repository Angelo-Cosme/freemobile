<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function get_reg(){
    
    global $db;
    
    $req = $db->query("
            SELECT register.id,
                   register.phone,
                   register.types_operations,
                   register.montant,
                   register.nom_prenom,
                   register.network,
                   register.ref_piece_id,
                   register.type_piece,
                   register.expire_piece,
                   register.date_operation
            FROM register
            WHERE register.id = '{$_GET['id']}'
        ");
    $result = $req->fetchObject();
    return $result;
}

function update($phone,$types_operations,$montant,$nom_prenom,$network,$ref_piece_id,$type_piece,$expire_piece,$date_operation,$id){
    
    global $db;
    
    $upd = [
      'phone' => $phone, 
      'types_operations' => $types_operations,
      'montant' => $montant,
      'nom_prenom' => $nom_prenom,
      'network' => $network,
      'ref_piece_id' => $ref_piece_id,
      'type_piece' => $type_piece,
      'expire_piece' => $expire_piece,
      'date_operation' => $date_operation,
      'id' => $id
   ];
    
    $sql = "UPDATE register SET phone=:phone, types_operations=:types_operations, montant=:montant, nom_prenom=:nom_prenom, network=:network, 
                                ref_piece_id=:ref_piece_id, type_piece=:type_piece, expire_piece=:expire_piece, date_operation=:date_operation
            WHERE id=:id";
    $req = $db->prepare($sql);
    $req->execute($upd);
}

