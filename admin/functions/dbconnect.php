<?php

    session_start();

    $dbhost = 'localhost';
    $dbname = 'freemobile';
    $dbuser = 'root';
    $dbpswd = '';
   

    try{
        $db = new PDO('mysql:host='.$dbhost.';dbname='.$dbname,$dbuser,$dbpswd,array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8', PDO::ATTR_ERRMODE => PDO::ERRMODE_WARNING));
    }catch(PDOexception $e){
        die("Une erreur est survenue lors de la connexion à la base de données");
    }

function admin(){
    if(isset($_SESSION['admin'])){
        global $db;
        
        $c = [
          'username' => $_SESSION['admin'],
          'role' => 'admin'
        ];
        
        $sql = "SELECT * FROM users WHERE username=:username AND role=:role";
        $req = $db->prepare($sql);
        $req->execute($c);
        $exit = $req->rowCount($sql);
        return $exit;
    }else{
        return 0;
    }
}

