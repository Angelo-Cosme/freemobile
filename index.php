<!DOCTYPE html>
<html>
    <head>
        <title></title>
        <meta charset="utf-8" />
        <link rel="stylesheet" href="admin/css/style.css">
    </head>
    <body>
        <div class="main">
            <nav>
                <div class="logo">
                    <!---<img src="img/logo.png">--->
                </div>
                <div class="nav-links">
                    <ul>
                        <li><a href="admin/index.php?page=login">Connexion</a></li>
                        <li><a href="#">A Propos</a></li>
                    </ul>
                </div>
            </nav>
            
            <div class="information">
                <div class="overlay"></div>
                <img src="img/mobile.png" class="mobile">
                <div id="circle">
                    <div class="feature one">
                        <div><h1>Rapidité</h1><p>Enregistrer vos informations</p></div>
                    </div>
                    <div class="feature two">
                        <div><h1>Numérisation</h1><p>Grâce à une base de données</p></div>
                    </div>
                    <div class="feature three">
                        <div><h1>Mobilité</h1><p>Exportable n'importe où</p></div>
                    </div>
                    <div class="feature four">
                        <div><h1>Sécurisation</h1><p>En toute sécurité</p></div> 
                    </div>
                </div>
            </div>
            
            <div class="controls">
                <img src="img/arrow.png" id="upBtn">
                <h3>MyFreeMobile / GPB</h3>
                <img src="img/arrow.png" id="downBtn">
            </div>
            
        </div>
        
        <script>
            var circle = document.getElementById("circle");
            var upBtn = document.getElementById("upBtn");
            var downBtn = document.getElementById("downBtn");
            
            var rotateValue = circle.style.transform;
            var rotateSum;
            
            upBtn.onclick = function()
            {
                rotateSum = rotateValue + "rotate(-90deg)";
                circle.style.transform = rotateSum;
                rotateValue = rotateSum;
            }
            
            downBtn.onclick = function()
            {
                rotateSum = rotateValue + "rotate(90deg)";
                circle.style.transform = rotateSum;
                rotateValue = rotateSum;
            }
        </script>
    </body>
    
    
</html>